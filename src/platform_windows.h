// This file is under GNU General Public License 3.0
// see LICENSE.txt

#pragma once

// Windows platform specifica

#define _EXPORT_PEP_ENGINE_DLL
#pragma warning(disable : 4996)

// We need to make sure winsock2 is included before windows.h, or we will get redefinitions of symbols
// as windows.h includes winsock1.h, so we will have duplicate symbols if windows.h is included first.
// It seems some of our code includes sync.h before including winsock.h, leading to the failure.
// Including winsock2.h here fixes the problem for now...
#ifdef WIN32 
#include <winsock2.h>
#endif // WIN32 

#include <Rpc.h>
#include <string.h>
#include <io.h>
#include <basetsd.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define ssize_t SSIZE_T
#define RTLD_LAZY 1
#define mode_t int

void *dlopen(const char *filename, int flag);
int dlclose(void *handle);
void *dlsym(void *handle, const char *symbol);
int mkstemp(char *templ);

time_t timegm(struct tm* tm);

#ifndef strdup
#define strdup(A) _strdup((A))
#endif
#ifndef snprintf
#if _MSC_VER<1900 // Includes undefined case. This is a check for VS 2015, which throws an error.
#define snprintf(...) _snprintf(__VA_ARGS__)
#endif
#endif
#ifndef strtok_r
#define strtok_r(A, B, C) strtok_s((A), (B), (C))
#endif
#ifndef strncasecmp
#define strncasecmp(A, B, C) _strnicmp((A), (B), (C))
#endif
#ifndef strcasecmp
#define strcasecmp(A, B) _stricmp((A), (B))
#endif
#ifndef gmtime_r
#define gmtime_r(A, B) gmtime_s((B), (A))
#endif
#ifndef ftruncate
#define ftruncate(A, B) _chsize((A), (B))
#endif
#ifndef ftello
#define ftello(A) ((off_t) _ftelli64(A))
#endif

char *strndup(const char *s1, size_t n);
char *stpcpy(char *dst, const char *src);

size_t strlcpy(char* dst, const	char* src, size_t size);
size_t strlcat(char* dst, const	char* src, size_t size);

const char *windoze_local_db(void);
const char *windoze_system_db(void);
const char *gpg_conf(void);
const char *gpg_agent_conf(void);

long random(void);

// on Windoze, uuid_t needs pointer semantics
typedef UUID pEpUUID[1];
#define _UUID_STRING_T
typedef char uuid_string_t[37];

void uuid_generate_random(pEpUUID out);
int uuid_parse(char *in, pEpUUID uu);
void uuid_unparse_upper(pEpUUID uu, uuid_string_t out);

#ifndef __cplusplus
#define inline __inline
#endif

#ifdef __cplusplus
}
#endif
