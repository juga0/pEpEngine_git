// This file is under GNU General Public License 3.0
// see LICENSE.txt

#include "trans_auto.h"

PEP_STATUS auto_sendto(PEP_SESSION session, const message *msg)
{

    return PEP_STATUS_OK;
}

PEP_STATUS auto_readnext(PEP_SESSION session, message **msg, PEP_transport_t **via)
{

    return PEP_STATUS_OK;
}
