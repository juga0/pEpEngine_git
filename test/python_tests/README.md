# Usage

To execute these tests, install p≡p Python adapter from
<https://letsencrypt.pep.foundation/dev/repos/pEpPythonAdapter/>

These tests are meant to be run using py.test <https://pytest.org> and
pytest-xdist <https://pypi.python.org/pypi/pytest-xdist>

You can run setup_test.py before running the tests if the initial setup is
failing.

To remove all generated files run setup_test.py -r

# License

All documentation is under CC BY-SA 3.0.
